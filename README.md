NJson
=====

Introduction
------------

This a json parser and build for the ghost butler project

The parser/builder engines are coming from

- https://github.com/udp/json-parser
- https://github.com/udp/json-builder

The implementation output an array of key/value with a specified value type

Dependencies
------------

This project depends on

- NLib
- NParser

Example
-------

Look at the NJson.c example to learn how to use.


Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NParser/NJson

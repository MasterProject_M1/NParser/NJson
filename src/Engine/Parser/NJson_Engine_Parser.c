#include "../../../include/NJson.h"

// -------------------------------
// namespace NJson::Engine::Output
// -------------------------------

/**
 * Process a json value
 *
 * @param value
 * 		The node
 * @param depth
 * 		The current depth
 * @param out
 * 		The output list
 * @param currentKey
 * 		The current key
 */
static void NJson_Engine_Output_ProcessValue( json_value *value,
	NU32 depth,
	__OUTPUT NParserOutputList *out,
	const char *currentKey );

/**
 * Process a json object
 *
 * @param value
 * 		The node
 * @param depth
 * 		The current depth
 * @param out
 * 		The output list
 * @param currentKey
 * 		The current key
 */
static void NJson_Engine_Output_ProcessObject( json_value *value,
	NU32 depth,
	__OUTPUT NParserOutputList *out,
	const char *currentKey )
{
	// Object member count
	NU32 length;

	// Iterator
	NU32 i = 0;

	// New key
	char *newKey;

	// Check value
	if( !value )
		return;

	// Save object member count
	length = value->u.object.length;

#define DEPTH_POINT_IGNORE		1

	// Process object members
	for( ; i < length; i++ )
	{
		// Create new key
		if( !( newKey = calloc( strlen( currentKey )
					+ ( ( depth > DEPTH_POINT_IGNORE ) ?
						1
						: 0 )
					+ strlen( value->u.object.values[ i ].name )
					+ 1,
				sizeof( char ) ) ) )
			continue;

		// Compose key
			// Old key
				memcpy( newKey,
					currentKey,
					strlen( currentKey ) );
			// .
				if( depth > DEPTH_POINT_IGNORE )
					memcpy( newKey + strlen( currentKey ),
						".",
						1 );
			// New key
				memcpy( newKey + strlen( currentKey ) + ( depth > DEPTH_POINT_IGNORE ?
						1
						: 0 ),
					value->u.object.values[ i ].name,
					strlen( value->u.object.values[ i ].name ) );

		// Process value
		//printf("object[%d].name = %s\n", i, value->u.object.values[ i ].name);
		NJson_Engine_Output_ProcessValue( value->u.object.values[ i ].value,
			depth + 1,
			out,
			newKey );

		// Free
		NFREE( newKey );
	}
#undef DEPTH_POINT_IGNORE
}

/**
 * Process a json array
 *
 * @param value
 * 		The node
 * @param depth
 * 		The current depth
 * @param out
 * 		The output list
 * @param currentKey
 * 		The current key
 */
static void NJson_Engine_Output_ProcessArray( json_value *value,
	NU32 depth,
	__OUTPUT NParserOutputList *out,
	const char *currentKey )
{
	// Array length
	NU32 length;

	// Iterator
	NU32 i = 0;

	// Check value
	if( !value )
		return;

	// Save array length
	length = value->u.array.length;

	// Process array values
	for ( ; i < length; i++ )
		// Process value
		NJson_Engine_Output_ProcessValue( value->u.array.values[ i ],
			depth,
			out,
			currentKey );
}

/**
 * Process a json value
 *
 * @param value
 * 		The node
 * @param depth
 * 		The current depth
 * @param out
 * 		The output list
 * @param currentKey
 * 		The current key
 */
static void NJson_Engine_Output_ProcessValue( json_value *value,
	NU32 depth,
	__OUTPUT NParserOutputList *out,
	const char *currentKey )
{
	// Value to create
	char *entryValue = NULL;

	// Iterator
	NS32 i;

	// Entry type
	json_type entryType;

	// Final entry type
	NParserOutputType outputType;

	// Check value
	if( !value )
		return;

	// Analyse type
	switch( entryType = value->type )
	{
		default:
		case json_none:
			break;

		case json_object:
			NJson_Engine_Output_ProcessObject( value,
				depth + 1,
				out,
				currentKey );
			break;
		case json_array:
			NJson_Engine_Output_ProcessArray( value,
				depth + 1,
				out,
				currentKey );
			break;
		case json_integer:
			// Allocate
			if( !( entryValue = calloc( 1,
				sizeof( NS32 ) ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Ignore
				break;
			}

			// Copy
			( *(NS32*)( entryValue ) ) = value->u.integer;
			break;
		case json_double:
			// Allocate
			if( !( entryValue = calloc( 1,
				sizeof( double ) ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Ignore
				break;
			}

			// Copy
			( *(double*)( entryValue ) ) = value->u.dbl;
			break;
		case json_string:
			// Allocate
			if( !( entryValue = calloc( value->u.string.length + 1,
				sizeof( char ) ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Ignore
				break;
			}

			// Copy
			memcpy( entryValue,
				value->u.string.ptr,
				value->u.string.length );
			break;
		case json_boolean:
			// Allocate
			if( !( entryValue = calloc( 1,
				sizeof( NBOOL ) ) ) )
			{
				// Notify
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Ignore
				break;
			}

			// Copy
			( *(NBOOL*)( entryValue ) ) = (NBOOL)value->u.boolean;
			break;
	}

	// There is entry to add
	if( entryValue != NULL )
	{
		// Get type
		switch( entryType )
		{
			case json_string:
				outputType = NPARSER_OUTPUT_TYPE_STRING;
				break;

			case json_boolean:
				outputType = NPARSER_OUTPUT_TYPE_BOOLEAN;
				break;

			default:
			case json_integer:
				outputType = NPARSER_OUTPUT_TYPE_INTEGER;
				break;

			case json_double:
				outputType = NPARSER_OUTPUT_TYPE_DOUBLE;
				break;
		}

		// Correct key
		if( currentKey[ 0 ] == '.' )
		{
			// Remove
			for( i = 0; i < (NS32)( strlen( currentKey ) - 1 ); i++ )
				( (char *)currentKey )[ i ] = currentKey[ i + 1 ];

			// Close
			( (char*)currentKey )[ strlen( currentKey ) - 1 ] = 0;
		}

		// Add entry
		NParser_Output_NParserOutputList_AddEntry( out,
			currentKey,
			entryValue,
			outputType );
	}
}

/**
 * Parse json string
 *
 * @param jsonContent
 *		The json content
 * @param contentSize
 * 		The json string length
 */
__ALLOC NParserOutputList *NJson_Engine_Parser_Parse( const char *jsonContent,
	NU32 contentSize )
{
	// Output
	__OUTPUT NParserOutputList *out;

	// Json value
	json_value *value;

	// Parse json
	if( !( value = json_parse( jsonContent,
		contentSize ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quit
		return NULL;
	}

	// Create output list
	if( !( out = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		json_value_free( value );

		// Quit
		return NULL;
	}

	// Process
	NJson_Engine_Output_ProcessValue( value,
		0,
		out,
		"" );

	// Free
	json_value_free( value );

	// OK
	return out;
}

/**
 * Parse json file
 *
 * @param filename
 *		The file name
 */
__ALLOC NParserOutputList *NJson_Engine_Parser_Parse2( const char *filename )
{
	// File
	NFichierBinaire *file;

	// File content
	char *fileContent;

	// Content size
	NU64 contentSize;

	// Output
	__OUTPUT NParserOutputList *out;

	// Open file
	if( !( file = NLib_Fichier_NFichierBinaire_ConstruireLecture( filename ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quit
		return NULL;
	}

	// Read file content
	contentSize = NLib_Fichier_NFichierBinaire_ObtenirTaille( file );
	if( !( fileContent = NLib_Fichier_NFichierBinaire_LireTout( file ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

		// Close
		NLib_Fichier_NFichierBinaire_Detruire( &file );

		// Quit
		return NULL;
	}

	// Close file
	NLib_Fichier_NFichierBinaire_Detruire( &file );

	// Generate output
	out = NJson_Engine_Parser_Parse( fileContent,
		(NU32)contentSize );

	// Free
	NFREE( fileContent );

	// OK?
	return out;
}



#include "../../../include/NJson.h"

// -------------------------------------------
// struct NJson::Engine::Builder::NBuilderPart
// -------------------------------------------

/**
 * Build the part
 *
 * @param value
 * 		The value
 * @param key
 * 		The key
 *
 * @return the part instance
 */
__ALLOC NBuilderPart *NJson_Engine_Builder_NBuilderPart_Build( json_value *value,
	const char *key )
{
	// Output
	__OUTPUT NBuilderPart *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NBuilderPart ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate key
	if( !( out->m_key = NLib_Chaine_Dupliquer( key ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_value = value;

	// OK
	return out;
}

/**
 * Destroy part
 *
 * @param this
 * 		This instance
 */
void NJson_Engine_Builder_NBuilderPart_Destroy( NBuilderPart **this )
{
	// Free
	NFREE( (*this)->m_key );
	NFREE( (*this) );
}

/**
 * Get the key
 *
 * @param this
 * 		This instance
 *
 * @return the key
 */
const char *NJson_Engine_Builder_NBuilderPart_GetKey( const NBuilderPart *this )
{
	return this->m_key;
}

/**
 * Get value
 *
 * @param this
 * 		This instance
 *
 *
 * @return the value
 */
json_value *NJson_Engine_Builder_NBuilderPart_GetValue( const NBuilderPart *this )
{
	return this->m_value;
}

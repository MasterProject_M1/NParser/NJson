#include "../../../include/NJson.h"

// --------------------------------
// namespace NJson::Engine::Builder
// --------------------------------

/**
 * Build json struct
 *
 * @param outputList
 * 		The parser output list
 * @param root
 * 		The json root element
 *
 * @return if operation succeeded
 */
NBOOL NJson_Engine_Builder_BuildJson( const NParserOutputList *outputList,
	json_value *root )
{
	// Iterators
	NU32 i = 0,
		j,
		k;

	// Entry
	const NParserOutput *entry;

	// Cut string list
	NListe *cutString;

	// Current element
	json_value *currentElement;

	// String piece
	const char *stringPiece;

	// Key
	char key[ 2048 ];

	// Value
	json_value *value = NULL;

	// Key/json value pair
	NBuilderPartList *pairList;

	// Builder pair
	const NBuilderPart *pair;

	// Build pair list
	if( !( pairList = NJson_Engine_Builder_NBuilderPartList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Iterate
	for( ; i < NParser_Output_NParserOutputList_GetEntryCount( outputList ); i++ )
	{
		// Get entry
		if( !( entry = NParser_Output_NParserOutputList_GetEntry( outputList,
			i ) ) )
			// Ignore
			continue;

		// Cut entry
		if( !( cutString = NLib_Chaine_Decouper( NParser_Output_NParserOutput_GetKey( entry ),
			'.' ) ) )
			// Ignore
			continue;

		// Check depth
		if( NLib_Memoire_NListe_ObtenirNombre( cutString ) == 0 )
		{
			// Clean
			NLib_Memoire_NListe_Detruire( &cutString );

			// Ignore
			continue;
		}

		// Iterate cut string
		currentElement = root;
		for( j = 0; j < NLib_Memoire_NListe_ObtenirNombre( cutString ); j++ )
		{
			// Get piece
			if( ( stringPiece = NLib_Memoire_NListe_ObtenirElementDepuisIndex( cutString,
				j ) ) == NULL )
				// Ignore
				continue;

			// Last element?
			if( (NS32)j >= (NS32)( NLib_Memoire_NListe_ObtenirNombre( cutString ) - 1 ) )
			{
				// Evaluate occurence count
				if( NParser_Output_NParserOutputList_CountKeyOccurence( outputList,
					NParser_Output_NParserOutput_GetKey( entry ),
					NFALSE ) > 1 )
				{
					// Empty key
					memset( key,
						0,
						2048 );

					// Copy
					for( k = 0; k <= j; k++ )
					{
						// Add
						strcat( key,
							NLib_Memoire_NListe_ObtenirElementDepuisIndex( cutString,
								k ) );

						// Last key?
						if( k < j )
							// Add separator
							strcat( key,
								"." );
					}

					// Do we already have a key/value pair?
					if( ( pair = NJson_Engine_Builder_NBuilderPartList_FindKey( pairList,
						key ) ) == NULL )
					{
						// Create array
						value = json_array_new( 0 );

						// Add to current level
						switch( currentElement->type )
						{
							case json_object:
								json_object_push( currentElement,
									stringPiece,
									value );
								break;
							case json_array:
								json_array_push( currentElement,
									value );
								break;

							default:
								break;
						}

						// Save current element
						currentElement = value;

						// Save new pair
						NJson_Engine_Builder_NBuilderPartList_AddKeyValue( pairList,
							key,
							currentElement );
					}
					else
						// Restore
						currentElement = NJson_Engine_Builder_NBuilderPart_GetValue( pair );
				}

				// Create value
				switch( NParser_Output_NParserOutput_GetType( entry ) )
				{
					case NPARSER_OUTPUT_TYPE_BOOLEAN:
						value = json_boolean_new( *(NBOOL *)NParser_Output_NParserOutput_GetValue( entry ) );
						break;
					case NPARSER_OUTPUT_TYPE_DOUBLE:
						value = json_double_new( *(double *)NParser_Output_NParserOutput_GetValue( entry ) );
						break;
					case NPARSER_OUTPUT_TYPE_INTEGER:
						value = json_integer_new( *(NBOOL *)NParser_Output_NParserOutput_GetValue( entry ) );
						break;
					case NPARSER_OUTPUT_TYPE_STRING:
						value = json_string_new( NParser_Output_NParserOutput_GetValue( entry ) );
						break;

					default:
						break;
				}

				// Add to current level
				switch( currentElement->type )
				{
					case json_object:
						json_object_push( currentElement,
							stringPiece,
							value );
						break;
					case json_array:
						json_array_push( currentElement,
							value );
						break;

					default:
						json_builder_free( value );
						break;
				}
			}
			// Not the last element
			else
			{
				// Empty key
				memset( key,
					0,
					2048 );

				// Copy
				for( k = 0; k <= j; k++ )
				{
					// Add
					strcat( key,
						NLib_Memoire_NListe_ObtenirElementDepuisIndex( cutString,
							k ) );

					// Last key?
					if( k < j )
						// Add separator
						strcat( key,
							"." );
				}

				// Look for key
				if( ( pair = NJson_Engine_Builder_NBuilderPartList_FindKey( pairList,
					key ) ) != NULL )
					// Save new root
					currentElement = NJson_Engine_Builder_NBuilderPart_GetValue( pair );
				else
				{
					// Create object
					value = json_object_new( 0 );

					// Add
					switch( currentElement->type )
					{
						case json_object:
							json_object_push( currentElement,
								stringPiece,
								value );
							break;
						case json_array:
							json_array_push( currentElement,
								value );
							break;

						default:
							json_builder_free( value );
							break;
					}

					// Add key/value
					NJson_Engine_Builder_NBuilderPartList_AddKeyValue( pairList,
						key,
						value );

					// Save new root
					currentElement = value;
				}
			}
		}

		// Clean
		NLib_Memoire_NListe_Detruire( &cutString );
	}

	// Destroy pair list
	NJson_Engine_Builder_NBuilderPartList_Destroy( &pairList );

	// OK
	return NTRUE;
}

/**
 * Build json string from output parser list
 *
 * @param outputList
 * 		The parser output list
 *
 * @return the json string
 */
__ALLOC char *NJson_Engine_Builder_Build( const NParserOutputList *parserOutputList )
{
	return NJson_Engine_Builder_Build2( parserOutputList,
		json_serialize_mode_multiline );
}

/**
 * Build json string from output parser list with specified style
 *
 * @param parserOutputList
 * 		The parser output list
 * @param style
 * 		The style (json_serialize_mode_multiline/json_serialize_mode_single_line/json_serialize_mode_packed)
 *
 * @return the json string
 */
__ALLOC char *NJson_Engine_Builder_Build2( const NParserOutputList *parserOutputList,
	NU32 style )
{
	// The root
	json_value *root;

	// Output
	__OUTPUT char *out;

	// Serialization options
	json_serialize_opts option;

	// Build root
	root = json_object_new( 0 );

	// Build json object
	NJson_Engine_Builder_BuildJson( parserOutputList,
		root );

	// Clean parameter
	memset( &option,
		0,
		sizeof( json_serialize_opts ) );

	// Set parameter
	option.mode = style;
	//option.opts = json_serialize_opt_no_space_after_colon
	//	| json_serialize_opt_no_space_after_comma;
	option.indent_size = 4;

	// Allocate memory
	if( !( out = calloc( json_measure_ex( root,
		option ),
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Clean
		json_builder_free( root );

		// Quit
		return NULL;
	}

	// Build json string
	json_serialize_ex( out,
		root,
		option );

	// Clean
	json_builder_free( root );

	// OK
	return out;
}

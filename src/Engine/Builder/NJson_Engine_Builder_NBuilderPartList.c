#include "../../../include/NJson.h"

// -----------------------------------------------
// struct NJson::Engine::Builder::NBuilderPartList
// -----------------------------------------------

/**
 * Build part list
 *
 * @return the part list instance
 */
__ALLOC NBuilderPartList *NJson_Engine_Builder_NBuilderPartList_Build( void )
{
	// Output
	__OUTPUT NBuilderPartList *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NBuilderPartList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build list
	if( !( out->m_list = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NJson_Engine_Builder_NBuilderPart_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy part list
 *
 * @param this
 * 		This instance
 */
void NJson_Engine_Builder_NBuilderPartList_Destroy( NBuilderPartList **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_list );

	// Free
	NFREE( (*this) );
}

/**
 * Add key/value pair
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NJson_Engine_Builder_NBuilderPartList_AddKeyValue( NBuilderPartList *this,
	const char *key,
	json_value *value )
{
	// Entry part
	NBuilderPart *entry;

	// Check parameter
	if( NJson_Engine_Builder_NBuilderPartList_FindKey( this,
		key ) != NULL )
		// Already present
		return NFALSE;

	// Create entry
	if( !( entry = NJson_Engine_Builder_NBuilderPart_Build( value,
		key ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add entry
	if( !NLib_Memoire_NListe_Ajouter( this->m_list,
		entry ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Find key/value pair
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 *
 * @return the part or NULL if can't be found
 */
const NBuilderPart *NJson_Engine_Builder_NBuilderPartList_FindKey( const NBuilderPartList *this,
	const char *key )
{
	// Iterator
	NU32 i = 0;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
		// This is the one?
		if( NLib_Chaine_Comparer( NJson_Engine_Builder_NBuilderPart_GetKey( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				i ) ),
			key,
			NTRUE,
			0 ) )
			// OK
			return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				i );

	// Can't be found
	return NULL;
}


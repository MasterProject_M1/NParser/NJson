#include "../include/NJson.h"

// ---------------
// namespace NJson
// ---------------

#ifdef TEST_JSON
#define TEST_FILE_COUNT		2

static const char TestFilePath[ TEST_FILE_COUNT ][ 32 ] =
{
	"test/test.json",
	"test/test2.json"
};

/**
 * Entry point for test
 *
 * @param argc
 * 		The argument count
 * @param argv
 * 		The argument vector
 *
 * @return EXIT_SUCCESS if OK
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Json output list
	NParserOutputList *outputList;

	// Entry
	NParserOutput *entry;

	// Builder output
	char *builderOutput;

	// Iterators
	NU32 i = 0,
		j;

	// Init NLib
	NLib_Initialiser( NULL );

	for( j = 0; j < TEST_FILE_COUNT; j++ )
	{
		// Load
		printf( "Parsing [%s]...\n\n",
			TestFilePath[ j ] );
		if( !( outputList = NJson_Engine_Parser_Parse2( TestFilePath[ j ] ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quit
			return EXIT_FAILURE;
		}

		// List
		for( i = 0; i < outputList->m_list->m_nombre; i++ )
		{
			// Get entry
			entry = (NParserOutput *)outputList->m_list->m_element[ i ];

			// Display
			printf( "[%s]:[",
				entry->m_key );
			switch( entry->m_type )
			{
				case NPARSER_OUTPUT_TYPE_INTEGER:
				case NPARSER_OUTPUT_TYPE_BOOLEAN:
					printf( "%d",
						*( (NS32 *)entry->m_value ) );
					break;
				case NPARSER_OUTPUT_TYPE_DOUBLE:
					printf( "%f",
						*( (double *)entry->m_value ) );
					break;
				case NPARSER_OUTPUT_TYPE_STRING:
					printf( "%s",
						entry->m_value );
					break;

				default:
					break;
			}
			printf( "][" );
			switch( entry->m_type )
			{
				case NPARSER_OUTPUT_TYPE_INTEGER:
					puts( "integer]" );
					break;
				case NPARSER_OUTPUT_TYPE_BOOLEAN:
					puts( "boolean]" );
					break;
				case NPARSER_OUTPUT_TYPE_DOUBLE:
					puts( "double]" );
					break;
				case NPARSER_OUTPUT_TYPE_STRING:
					puts( "string]" );
					break;

				default:
					break;
			}
		}

		// Build
		printf( "\nRebuilding %s from previous parser result...\n\n",
			TestFilePath[ j ] );
		if( ( builderOutput = NJson_Engine_Builder_Build( outputList ) ) )
		{
			// Build result:
			puts( builderOutput );

			// Free
			NFREE( builderOutput );
		}
		else
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Destroy
			NParser_Output_NParserOutputList_Destroy( &outputList );

			// Quit
			return EXIT_FAILURE;
		}

		// Destroy
		NParser_Output_NParserOutputList_Destroy( &outputList );

		// Separate
		puts( "\n" );
	}

	// Quit NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
#endif // TEST_JSON


#ifndef NJSON_ENGINE_BUILDER_PROTECT
#define NJSON_ENGINE_BUILDER_PROTECT

// --------------------------------
// namespace NJson::Engine::Builder
// --------------------------------

// struct NJson::Engine::Builder::NBuilderPart
#include "NJson_Engine_Builder_NBuilderPart.h"

// struct NJson::Engine::Builder::NBuilderPartList
#include "NJson_Engine_Builder_NBuilderPartList.h"

/**
 * Build json string from output parser list
 *
 * @param parserOutputList
 * 		The parser output list
 *
 * @return the json string
 */
__ALLOC char *NJson_Engine_Builder_Build( const NParserOutputList *parserOutputList );

/**
 * Build json string from output parser list with specified style
 *
 * @param parserOutputList
 * 		The parser output list
 * @param style
 * 		The style (json_serialize_mode_multiline/json_serialize_mode_single_line/json_serialize_mode_packed)
 *
 * @return the json string
 */
__ALLOC char *NJson_Engine_Builder_Build2( const NParserOutputList *parserOutputList,
	NU32 style );

#endif // !NJSON_ENGINE_BUILDER_PROTECT


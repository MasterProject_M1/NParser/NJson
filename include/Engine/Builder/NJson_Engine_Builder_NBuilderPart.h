#ifndef NJSON_ENGINE_BUILDER_NBUILDERPART_PROTECT
#define NJSON_ENGINE_BUILDER_NBUILDERPART_PROTECT

// -------------------------------------------
// struct NJson::Engine::Builder::NBuilderPart
// -------------------------------------------

typedef struct NBuilderPart
{
	// The associated json value
	json_value *m_value;

	// The key
	char *m_key;
} NBuilderPart;

/**
 * Build the part
 *
 * @param value
 * 		The value
 * @param key
 * 		The key
 *
 * @return the part instance
 */
__ALLOC NBuilderPart *NJson_Engine_Builder_NBuilderPart_Build( json_value *value,
	const char *key );

/**
 * Destroy part
 *
 * @param this
 * 		This instance
 */
void NJson_Engine_Builder_NBuilderPart_Destroy( NBuilderPart** );

/**
 * Get the key
 *
 * @param this
 * 		This instance
 *
 * @return the key
 */
const char *NJson_Engine_Builder_NBuilderPart_GetKey( const NBuilderPart* );

/**
 * Get value
 *
 * @param this
 * 		This instance
 *
 *
 * @return the value
 */
json_value *NJson_Engine_Builder_NBuilderPart_GetValue( const NBuilderPart* );

#endif // !NJSON_ENGINE_BUILDER_NBUILDERPART_PROTECT



#ifndef NJSON_ENGINE_BUILDER_NBUILDERPARTLIST_PROTECT
#define NJSON_ENGINE_BUILDER_NBUILDERPARTLIST_PROTECT

// -----------------------------------------------
// struct NJson::Engine::Builder::NBuilderPartList
// -----------------------------------------------

typedef struct NBuilderPartList
{
	// Builder part list
	NListe *m_list;
} NBuilderPartList;

/**
 * Build part list
 *
 * @return the part list instance
 */
__ALLOC NBuilderPartList *NJson_Engine_Builder_NBuilderPartList_Build( void );

/**
 * Destroy part list
 *
 * @param this
 * 		This instance
 */
void NJson_Engine_Builder_NBuilderPartList_Destroy( NBuilderPartList** );

/**
 * Add key/value pair
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NJson_Engine_Builder_NBuilderPartList_AddKeyValue( NBuilderPartList*,
	const char *key,
	json_value *value );

/**
 * Find key/value pair
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 *
 * @return the part or NULL if can't be found
 */
const NBuilderPart *NJson_Engine_Builder_NBuilderPartList_FindKey( const NBuilderPartList*,
	const char *key );

#endif // !NJSON_ENGINE_BUILDER_NBUILDERPARTLIST_PROTECT

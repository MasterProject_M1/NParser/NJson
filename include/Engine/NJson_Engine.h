#ifndef NJSON_ENGINE_PROTECT
#define NJSON_ENGINE_PROTECT

// -----------------------
// namespace NJson::Engine
// -----------------------

// namespace NJson::Engine::json_parser
#include "json/NJson_Engine_json_parser.h"

// namespace NJson::Engine::json_builder
#include "json/NJson_Engine_json_builder.h"

// namespace NJson::Engine::Parser
#include "Parser/NJson_Engine_Parser.h"

// namespace NJson::Engine::Builder
#include "Builder/NJson_Engine_Builder.h"

#endif // !NJSON_ENGINE_PROTECT


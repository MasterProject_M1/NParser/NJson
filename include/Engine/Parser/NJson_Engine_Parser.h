#ifndef NJSON_ENGINE_PARSER_PROTECT
#define NJSON_ENGINE_PARSER_PROTECT

// -------------------------------
// namespace NJson::Engine::Parser
// -------------------------------

/**
 * Parse json string
 *
 * @param jsonContent
 *		The json content
 * @param contentSize
 * 		The json string length
 */
__ALLOC NParserOutputList *NJson_Engine_Parser_Parse( const char *jsonContent,
	NU32 contentSize );

/**
 * Parse json file
 *
 * @param filename
 *		The file name
 */
__ALLOC NParserOutputList *NJson_Engine_Parser_Parse2( const char *filename );

#endif // !NJSON_ENGINE_PARSER_PROTECT


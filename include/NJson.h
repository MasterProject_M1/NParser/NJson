#ifndef NJSON_PROTECT
#define NJSON_PROTECT

// ---------------
// namespace NJson
// ---------------

// namespace NLib
#include "../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NParser
#include "../../NParser/include/NParser.h"

// namespace NJson::Engine
#include "Engine/NJson_Engine.h"

#endif // !NJSON_PROTECT
